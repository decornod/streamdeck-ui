Source: streamdeck-ui
Section: utils
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Benjamin Drung <bdrung@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               libhidapi-dev,
               libxcb-xinerama0-dev,
               python3-all,
               python3-elgato-streamdeck,
               python3-pil,
               python3-pynput,
               python3-pyside2.qtcore,
               python3-pyside2.qtgui,
               python3-pyside2.qtwidgets,
               python3-setuptools,
               python3-xlib
Standards-Version: 4.6.2
Homepage: https://github.com/timothycrosley/streamdeck-ui
Vcs-Browser: https://salsa.debian.org/python-team/packages/streamdeck-ui
Vcs-Git: https://salsa.debian.org/python-team/packages/streamdeck-ui.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: streamdeck-ui
Architecture: all
Depends: fonts-roboto-unhinted,
         libhidapi-libusb0,
         ${misc:Depends},
         ${python3:Depends}
Description: Linux compatible UI for the Elgato Stream Deck
 streamdeck_ui is a Linux compatible UI for the Elgato Stream Deck.
 Key Features:
  * Multi-device: Enables connecting and configuring multiple Stream Deck
    devices on one computer.
  * Brightness Control: Supports controlling the brightness from both the
    configuration UI and buttons on the device itself.
  * Configurable Button Display: Icons + Text, Icon Only, and Text Only
    configurable per button on the Stream Deck.
  * Multi-Action Support: Run commands, write text and press hotkey combinations
    at the press of a single button on your Stream Deck.
  * Button Pages: streamdeck_ui supports multiple pages of buttons and
    dynamically setting up buttons to switch between those pages.
  * Auto Reconnect: Automatically and gracefully reconnects, in the case the
    device is unplugged and replugged in.
  * Import/Export: Supports saving and restoring Stream Deck configuration.
